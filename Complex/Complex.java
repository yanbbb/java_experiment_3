import java.text.DecimalFormat;//引入DecimalFormat包取一位整数和一位小数

public class Complex {
    double Real=0;
    double Imaginary=0;
    public Complex(){}
    public Complex(double Real,double Imaginary){
        this.Real=Real;
        this.Imaginary=Imaginary;

    }
    public double getReal(){
        return Real;
    }
    public double getImaginary(){
        return Imaginary;
    }
    public String toString(){
        String s = "";
        double r=Real;
        double i=Imaginary;
        if(r==0&&i==0){
            s="0";
        }
        else if(r==0&&i!=0){
            s=i+"i";
        }
        else if(r!=0&&i<0){
            s=r+""+i+"i";
        }
        else if(r!=0&&i==0){
            s=r+"";
        }
        else
        {
            s=r+"+"+i+"i";
        }
        return s;
    }
    public boolean equals(Object obj){//重写equals方法，使其不用来对比字符序列
        if(this==obj){
            return true;
        }
        else
            return false;
    }
    DecimalFormat df = new DecimalFormat( "0.0");
    public Complex ComplexAdd(Complex a){
        return new Complex(Real+a.getReal(),Imaginary+a.getImaginary());
    }
    public Complex ComplexSub(Complex a){
        return new Complex(Real-a.getReal(),Imaginary-a.getImaginary());
    }
    public Complex ComplexMulti(Complex a){
        double r=Real*a.getReal()-Imaginary*a.getImaginary();
        double i =Imaginary*a.getReal()+Real*a.getImaginary();
        return new Complex(Double.valueOf(df.format(r)),Double.valueOf(df.format(i)));
    }
    public Complex ComplexDiv(Complex a){
        double r=(Real * a.Imaginary + Imaginary * a.Real) / (a.Imaginary * a.Imaginary + a.Real * a.Real);
        double i=(Imaginary * a.Imaginary + Real * a.Real) / (a.Real * a.Real + a.Real * a.Real);
        return new Complex(Double.valueOf(df.format(r)),Double.valueOf(df.format(i)));
    }
}
